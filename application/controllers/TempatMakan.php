<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TempatMakan extends CI_Controller {
	function __construct() {
		parent::__construct();
		// $this->load->model('admin_model');
		$this->load->model('Crud_model');
		$this->load->library('form_validation');
		$this->load->helper('url_helper');
		$this->load->helper('form');
	}

	public $judul = "Aplikasi Rekomendasi Pariwisata";

	public function table(){
		$this->data_table = [
			'id_tmakan',
			'nama_tmakan',
			'harga_tmakan',
			'latitude',
			'longitude'
		];

		$this->data_table_alias = [
			'ID',
			'Nama Tempat Makan',
			'Harga',
			'Latitude',
			'Longitude'
		];

	}

  public function lihat(){
    if ($this->session->userdata('username')) {
			$this->table();
			$data['isi'] = 't_makan/lihat';
			$data['title'] = $this->judul;
			$data['dt'] = $this->data_table;
			$data['dt_alias'] = $this->data_table_alias;
			$data['form_act'] = 'TempatMakan';
			$data['d'] = $this->Crud_model->index('tempat_makan')->result_array();
      $this->load->view('templates/themes', $data);
    }
    else{
      redirect('login');
    }
  }

  public function tambah(){
		if ($this->session->userdata('username')) {

			$data['isi'] = 't_makan/lihat';
			$data['title'] = $this->judul;
			$kata = array('is_unique' => '<b> "%s" </b> Sudah Digunakan ');
			$this->form_validation->set_rules('id_tmakan','ID','required|is_unique[tempat_makan.id_tmakan]',$kata);

			if ($this->form_validation->run()) {
				$this->table();
				$d     = $this->data_table;
				$count = sizeof($d);

				for ($i=0; $i < $count; $i++) {
					$data_input[$d[$i]] = $this->input->post($d[$i]);
				}

				$this->Crud_model->store('tempat_makan', $data_input);

				$this->session->set_flashdata('success_msg', 'Berhasil Ditambahkan');
        redirect('TempatMakan/lihat');
	    }
			else {
				 redirect('TempatMakan/lihat');
	    	$this->load->view('templates/themes', $data);
	    }
		}
		else{
			redirect('login');
		}
}

	public function update($id){
		if ($this->session->userdata('username')) {
			$this->table();
			$data['dt'] = $this->data_table;
			$data['dt_alias'] = $this->data_table_alias;
			$data['isi'] = 't_makan/update';
			$data['title'] = $this->judul;
			$data['form_act'] = 'TempatMakan';
			$this->form_validation->set_rules('nama_tmakan','Nama Tempat Makan','required');

			if ($this->form_validation->run() === FALSE) {
				$data['d'] = $this->Crud_model->select_where('tempat_makan','id_tmakan',$id)->row_array();
				$this->load->view('templates/themes', $data);
			}else {

				$d     = $this->data_table;
				$count = sizeof($d);

				$data_update['id_tmakan'] = $id;
				for ($i=0; $i < $count; $i++) {
					$data_update[$d[$i]] = $this->input->post($d[$i]);
				}

				$this->Crud_model->update('tempat_makan', $data_update);
				$this->session->set_flashdata('success_msg', 'Data Berhasil Di Perbaharui');
				redirect('TempatMakan/lihat');
			}
		}
		else{
			redirect('login');
		}
	}

	public function delete($id){
		$this->Crud_model->delete('tempat_makan','id_tmakan',$id);
		$this->session->set_flashdata('success_msg', 'Data Berhasil Di Hapus');
    redirect('TempatMakan/lihat');
  }

}
