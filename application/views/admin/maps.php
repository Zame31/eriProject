<?php
  $data_makan = null;
  $data_wisata = null;
  $data_menginap = null;

  foreach ($d_makan as $dm) {
    $data_makan = $data_makan."['".$dm["nama_tmakan"]."',".$dm["latitude"].",".$dm["longitude"]."],";
  }
  foreach ($d_wisata as $dw) {
    $data_wisata = $data_wisata."['".$dw["nama_twisata"]."',".$dw["latitude"].",".$dw["longitude"]."],";
  }
  foreach ($d_menginap as $dme) {
    $data_menginap = $data_menginap."['".$dme["nama_tmenginap"]."',".$dme["latitude"].",".$dme["longitude"]."],";
  }

?>
<main>
    <div id="map"></div>
    <script>
    function initMap() {
       var map = new google.maps.Map(document.getElementById('map'), {
         zoom: 10,
         center: {lat: -6.410444, lng: 106.107742}
       });

       setMarkers(map);
     }

     var data_makan = [ <?php echo $data_makan; ?> ];
     var data_wisata = [ <?php echo $data_wisata; ?> ];
     var data_menginap = [ <?php echo $data_menginap; ?> ];

     function setMarkers(map) {
       var image_makan = {
         url: '<?php echo base_url() ?>assets/img/ic_grocery.png',
         size: new google.maps.Size(40, 40),
         origin: new google.maps.Point(0, 0),
         anchor: new google.maps.Point(0, 32)
       };

       var image_wisata = {
         url: '<?php echo base_url() ?>assets/img/ic_sunbed.png',
         size: new google.maps.Size(40, 40),
         origin: new google.maps.Point(0, 0),
         anchor: new google.maps.Point(0, 32)
       };

       var image_menginap = {
         url: '<?php echo base_url() ?>assets/img/ic_hotel.png',
         size: new google.maps.Size(40, 40),
         origin: new google.maps.Point(0, 0),
         anchor: new google.maps.Point(0, 32)
       };

       for (var i = 0; i < data_makan.length; i++) {
         var d_makan = data_makan[i];
         var marker_makan = new google.maps.Marker({
           position: {lat: d_makan[1], lng: d_makan[2]},
           map: map,
           icon: image_makan,
           title: d_makan[0]
         });
       }

       for (var j = 0; j < data_wisata.length; j++) {
         var d_wisata = data_wisata[j];
         var marker_wisata = new google.maps.Marker({
           position: {lat: d_wisata[1], lng: d_wisata[2]},
           map: map,
           icon: image_wisata,
           title: d_wisata[0]
         });
       }

       for (var k = 0; k < data_menginap.length; k++) {
         var d_menginap = data_menginap[k];
         var marker_menginap = new google.maps.Marker({
           position: {lat: d_menginap[1], lng: d_menginap[2]},
           map: map,
           icon: image_menginap,
           title: d_menginap[0]
         });
       }
     }


    // function initMap() {
    //   var uluru = {lat: -25.363, lng: 131.044};
    //   var map = new google.maps.Map(document.getElementById('map'), {
    //     zoom: 4,
    //     center: uluru
    //   });
    //
    //   var contentString = '<div id="content">'+
    //       '<div id="siteNotice">'+
    //       '</div>'+
    //       '<h6 id="firstHeading" class="firstHeading">Tempat Makan</h6>';
    //
    //   var infowindow = new google.maps.InfoWindow({
    //     content: contentString
    //   });
    //
    //   var marker = new google.maps.Marker({
    //     position: uluru,
    //     map: map,
    //     title: 'Uluru (Ayers Rock)'
    //   });
    //   marker.addListener('click', function() {
    //     infowindow.open(map, marker);
    //   });
    // }
    //

    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyAQjbSD_aJBZSiQLOzKULbIHz6u6TP_Rsg&callback=initMap"></script>

</main>
