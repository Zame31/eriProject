<?php if ($this->session->flashdata('success_msg')) { ?>
      <div id="snackbar"> <?php echo $this->session->flashdata('success_msg') ?> </div>
  <?php } ?>
<main>
  <div class="title">
    <span><?php echo $title; ?></span>
      <div class="col s12 bred">
        <a href="#!" class="breadcrumb">Data</a>
        <a href="#!" class="breadcrumb">Admin</a>
      </div>
  </div>
  <nav class="teal">
    <div class="nav-wrapper ">
      <form>
        <div class="input-field">
          <input id="searchbox" type="search" required>
          <label for="searchbox" class="active"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
    </div>
  </nav>

  <div class="content ">
    <div class="card-panel white lighten-2">
      <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Tambah Data</a>
    <table class="highlight striped" id="example">
      <thead>
        <tr>
          <th>No</th>
          <th>Username</th>
          <th>Nama Lengkap</th>
          <th>E-Mail</th>
          <th>Akses</th>
          <th width="80px">Control</th>
        </tr>
      </thead>
      <tbody>

        <?php
        $i=1;
        if ($admin > 0 ) {

        foreach ($admin as $news_item) { ?>
        <tr>
          <!-- atribut di table database -->
          <td align="center"><?php echo $i; ?></td>
          <td><?php echo $news_item->username; ?></td>
          <td><?php echo $news_item->nama_lengkap; ?></td>
          <td><?php echo $news_item->email; ?></td>
          <td><?php echo $news_item->akses; ?></td>
          <td>
            <a class="waves-effect waves-light btn blue lighten-1 pad" href="<?php echo site_url('admin/update/'.$news_item->id_admin); ?>">
              <i class="material-icons">update</i>
            </a>
            <a href="#modal<?php echo $news_item->id_admin ?>" class="btn red lighten-2 modal-trigger waves-effect waves-light pad">
              <i class="material-icons">delete</i>
            </a>
          </td>
        </tr>

        <div id="modal<?php echo $news_item->id_admin ?>" class="delete modal">
          <div class="modal-content">
            <h6>anda yakin ingin menghapus data ini ?</h6>
          </div>
          <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat" >Tidak</a>
            <a href="<?php echo site_url('admin/delete/'.$news_item->id_admin); ?>" class=" modal-action waves-effect waves-green btn-flat">Iya</a>
          </div>
        </div>

          <?php $i++;} } ?>
        </tbody>
    </table>
  </div>
  </div>

  <?php if (validation_errors()) { ?>
        <div id="snackbar"> <?php echo validation_errors(); ?> </div>
    <?php } ?>
  <?php
  $attributes = array('onsubmit' => "return validate();");
  echo form_open('admin/tambah',$attributes);
  ?>

  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
      <div class="row form">
        <div class="col s12">
          <div class="card-panel cus-tambah white lighten-2">
            <p class="sub-tit teal lighten-2">Data Diri</p>
            <div class="content">
              <div class="row">
                <div class="input-field col s12">
                  <label>Nama Lengkap :</label>
                  <input id="val1" type="text" name="nama_lengkap" value="<?php echo set_value('nama_lengkap'); ?>">
                </div>
                <div class="input-field col s12">
                  <textarea name="alamat" id="val2" class="materialize-textarea"><?php echo set_value('alamat'); ?></textarea>
                  <label for="val3">Alamat</label>
                </div>
                <div class="input-field col s12">
                  <label>E-Mail :</label>
                  <input id="val4" type="text" name="email" value="<?php echo set_value('email'); ?>">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col s12">
          <div class="card-panel cus-tambah white lighten-2">
            <p class="sub-tit teal lighten-2">Admin </p>
            <div class="content">
              <div class="row">
                <div class="input-field col s12">
                  <select name="akses">
                    <option value="super_admin" selected>Super Admin</option>
                    <option value="admin_datin">Admin Datin</option>
                    <option value="admin_pkl">Admin PKL</option>
                  </select>
                  <label>Hak Akses :</label>
                </div>
                <div class="input-field col s12">
                  <label>Username :</label>
                  <input id="val5" type="text" name="username" value="<?php echo set_value('username'); ?>">
                </div>
                <div class="input-field col s6">
                  <label>Password :</label>
                  <input id="password" type="password" name="password">
                </div>
                <div class="input-field col s6">
                  <label>Ketik Ulang Password :</label>
                  <input id="re-password" type="password" name="re_password">
                </div>
              </div>
            </div>
          </div>
        </div>

          <!-- <div class="col s12">
            <button class="btn waves-effect waves-light btn-large" type="submit" name="action">Tambahkan
              <i class="material-icons right">send</i>
            </button>
          </div> -->
      </div>
    </div>
    <div class="modal-footer">
      <button class="btn waves-effect waves-light btn-flat" type="submit" name="action">Tambahkan
        <i class="material-icons right">send</i>
      </button>
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Batal</a>

    </div>
  </div>


</main>

<?php echo form_close();?>

<!-- Validasi Password-->
<script type="text/javascript">
    window.onload = function () {
    document.getElementById("password").onchange = validatePassword;
    document.getElementById("re-password").onchange = validatePassword;
    }
    function validatePassword(){
    var pass2=document.getElementById("re-password").value;
    var pass1=document.getElementById("password").value;
    if(pass1!=pass2)
    document.getElementById("re-password").setCustomValidity("Passwords Tidak Sama");
    else
    document.getElementById("re-password").setCustomValidity('');
	}
</script>
