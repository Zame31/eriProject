<ul class="side-nav fixed" id="scroll">
  <li>
    <div class="userView">
    <div class="background"></div>
      <a href="#!user" class="logo"><img class="responsive-img" src="<?php echo base_url() ?>assets/img/logo.png"></a>
      <span class="sub-title-web">Pariwisata <br> Provinsi Banten</span>
    </div>
  </li>
  <li>
    <div class="userView user">
      <div class="background teal lighten-1"></div>
      <a href="#" class="icon-menu">
        <span class="nama">
          <i class="material-icons">account_circle</i><?php echo $this->session->userdata('username') ?>
        </span>
      </a>
    </div>
  </li>

  <li class="no-padding">
    <ul class="collapsible collapsible-accordion">
      <!-- BERANDA -->
      <li class="<?php if($this->uri->uri_string() == 'penelitian/beranda') {echo 'grey lighten-3';} ?>">
        <a href="<?php echo site_url('admin/maps');?>" class="icon-menu">
          <i class="material-icons">home</i>Peta
        </a>
      </li>

      <!-- ADMIN -->
      <li class="<?php if($this->uri->uri_string() == 'admin/lihat') {echo 'grey lighten-3';} ?>">
        <a class="collapsible-header waves-effect waves-teal icon-menu">
          <i class="material-icons">account_circle</i>
          Data</a>
        <div class="collapsible-body">
          <ul>
            <li><a href="<?php echo site_url('admin/lihat'); ?>">Admin</a></li>
            <li><a href="<?php echo site_url('TempatMakan/lihat'); ?>">Tempat Makan</a></li>
            <li><a href="<?php echo site_url('TempatMenginap/lihat'); ?>">Tempat Menginap</a></li>
            <li><a href="<?php echo site_url('TempatWisata/lihat'); ?>">Tempat Wisata</a></li>


            <!-- <li><a href="<?php echo site_url('admin/tambah'); ?>">Tambah Admin</a></li> -->
          </ul>
        </div>
      </li>

      <!-- LOGOUT -->
      <li>
        <a href="<?php echo site_url('login/logout'); ?>" class="icon-menu"><i class="material-icons">power_settings_new</i>Logout</a>
      </li>
    </ul>
  </li>
</ul>
