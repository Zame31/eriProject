<?php if ($this->session->flashdata('success_msg')) { ?>
      <div id="snackbar"> <?php echo $this->session->flashdata('success_msg') ?> </div>
  <?php } ?>
  <?php
    $count_alias = sizeof($dt_alias);
    $count = sizeof($dt);
  ?>
<main>
  <div class="title">
    <span><?php echo $title; ?></span>
      <div class="col s12 bred">
        <a href="#!" class="breadcrumb">Data</a>
        <a href="#!" class="breadcrumb">Tempat Wisata</a>
      </div>
  </div>
  <nav class="teal">
    <div class="nav-wrapper ">
      <form>
        <div class="input-field">
          <input id="searchbox" type="search" required>
          <label for="searchbox" class="active"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
    </div>
  </nav>

  <div class="content ">
    <div class="card-panel white lighten-2">
      <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Tambah Data</a>
    <table class="highlight striped" id="example">
      <thead>
        <tr>
          <th>No</th>
          <?php
            for ($j=0; $j < $count_alias ; $j++) { ?>
              <th><?php echo $dt_alias[$j] ?></th>
            <?php } ?>
          <th width="80px">Control</th>
        </tr>
      </thead>
      <tbody>

        <?php $ii=0; ?>
        <?php foreach ($d as $data): ?>
          <tr>
            <td><?php echo $ii+1; ?></td>
            <?php
              for ($i=0; $i < $count ; $i++) { ?>
                <td><?php echo $data[$dt[$i]] ?></td>
              <?php } ?>
            <td>
              <a class="waves-effect waves-light btn blue lighten-1 pad" href="<?php echo site_url($form_act.'/update/'.$data[$dt[0]]); ?>">
                <i class="material-icons">update</i>
              </a>
              <a href="#modal<?php echo $data[$dt[0]] ?>" class="btn red lighten-2 modal-trigger waves-effect waves-light pad">
                <i class="material-icons">delete</i>
              </a>
            </td>
          </tr>
          <div id="modal<?php echo $data[$dt[0]] ?>" class="delete modal">
            <div class="modal-content">
              <h6>anda yakin ingin menghapus data ini ?</h6>
            </div>
            <div class="modal-footer">
              <a href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat" >Tidak</a>
              <a href="<?php echo site_url($form_act.'/delete/'.$data[$dt[0]]); ?>" class=" modal-action waves-effect waves-green btn-flat">Iya</a>
            </div>
          </div>

          <?php $ii++; ?>
        <?php endforeach; ?>

        </tbody>
    </table>
  </div>
  </div>

  <?php if (validation_errors()) { ?>
        <div id="snackbar"> <?php echo validation_errors(); ?> </div>
    <?php } ?>
  <?php
  $attributes = array('onsubmit' => "return validate();");
  echo form_open($form_act.'/tambah',$attributes);
  ?>

  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
      <div class="row form">
        <div class="col s12">
          <div class="card-panel cus-tambah white lighten-2">
            <p class="sub-tit teal lighten-2">Tambah Data</p>
            <div class="content">
              <div class="row">
                <?php
                for ($l=0; $l < $count ; $l++) { ?>
                  <div class="input-field col s12">
                    <label><?php echo $dt_alias[$l] ?> :</label>
                    <input type="text" name="<?php echo $dt[$l] ?>" value="<?php echo set_value($dt[$l]); ?>" required>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button class="btn waves-effect waves-light btn-flat" type="submit" name="action">Tambahkan
        <i class="material-icons right">send</i>
      </button>
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Batal</a>

    </div>
  </div>

</main>

<?php echo form_close();?>
