<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function index($table){
        return $this->db->get($table);
    }

    function store($table, $data){
        return $this->db->insert($table, $data);

    }
    function last_id()
    {
      $insert_id = $this->db->insert_id();
      return  $insert_id;
    }

    function update($table, $data){
      return $this->db->replace($table, $data);
    }

    function select_where($table, $key, $id){
      return $query = $this->db->get_where($table, array($key => $id));
    }


    function delete($table, $key, $id){
      return $this->db->delete($table, array($key => $id));
    }

    function join_1($select, $table1, $table2, $on){

        $this->db->select($select);
        $this->db->from($table1);
        $this->db->join($table2, $on);

        return $this->db->get();
    }    

    public function custom($sql)
    {
      return $this->db->query($sql);
    }

}

?>
