<?php if (validation_errors()) { ?>
      <div id="snackbar"> <?php echo validation_errors(); ?> </div>
  <?php } ?>
<?php
$count_alias = sizeof($dt_alias);
$count = sizeof($dt);
$attributes = array('onsubmit' => "return validate();");
echo form_open($form_act.'/update/'.$d[$dt[0]],$attributes);
?>
<input type="hidden" name="<?php echo $dt[0] ?>" value="<?php echo $d[$dt[0]]; ?>">
<main>
  <div class="title">
    <span><?php echo $title; ?></span>
      <div class="col s12 bred">
        <a href="#!" class="breadcrumb">Data</a>
        <a href="#!" class="breadcrumb">Update Tempat Menginap</a>
      </div>
  </div>
  <div class="content">
    <div class="row form">
      <div class="col s12">
        <div class="card-panel cus-tambah white lighten-2">
          <p class="sub-tit teal lighten-2">Update Data</p>
          <div class="content">
            <div class="row">
              <?php
              for ($l=0; $l < $count ; $l++) { ?>
                <div class="input-field col s12">
                  <label><?php echo $dt_alias[$l] ?> :</label>
                  <?php if ($l == 0): ?>
                    <input type="text" name="<?php echo $dt[$l] ?>" value="<?php echo $d[$dt[$l]]; ?>" disabled>
                  <?php else: ?>
                    <input type="text" name="<?php echo $dt[$l] ?>" value="<?php echo $d[$dt[$l]]; ?>" required>
                  <?php endif; ?>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>

        <div class="col s12">
          <button class="btn waves-effect waves-light btn-large" type="submit" name="action">Update
            <i class="material-icons right">send</i>
          </button>
        </div>
    </div>
  </div>
</main>
<?php echo form_close();?>
